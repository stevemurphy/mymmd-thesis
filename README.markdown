README

MMD metadata input files for Scrivener/MultiMarkdown Composer exports (to produce LaTeX thesis/drafts)

Created by Stephen Murphy on 2011-04-20

# Folder Contents

This folder is versioned with git.

Note that style files, BibTeX files etc would normally be in the texmf tree.

## Thesis version

LaTeX template files for writing the dissertation or for other academic/research writing.

These files will be used in MMD metadata, typically for compiling from Scrivener 2 or MultiMarkdown Composer. They produce the same result as if the standard template.tex from the latexthesis repo is used.

Example use:

    latex input:        my-mmd-thesis-header 
    Title:              My Document 
    Author:             Stephen Murphy
    Keywords:           draft, thesis
    Base Header Level:  2 
    LaTeX Mode:         memoir 
    latex input:        my-mmd-thesis-setup
    latex input:        my-mmd-thesis-begin-doc
    latex footer:       my-mmd-thesis-footer
    
    Some **markdown formatted** text.  
    LaTeX commands can be used directly within HTML comments:  
    <!--\include{exported}-->

Some files such as preamble.tex will need to be included if not in the texmf tree (see thesispreamble repo).


## Basic version

A new, basic version of the files, not necessarily for the thesis, has now been added. The simpler setup does not use the thesis style or fonts. Both bibtex and biblatex versions can be created. These are suitable for one-off documents such as drafts for review. Footer options include a bibliography, fixme notes, or both (or none).

### Biblatex example

Example markdown document:


    latex input:        my-mmd-basic-header 
    Title:              My Document 
    Author:             Stephen Murphy
    Keywords:           draft, thesis
    Base Header Level:  2 
    LaTeX Mode:         memoir 
    latex input:        my-mmd-basic-setup  
    latex input:        my-mmd-basic-biblatex  
    latex input:        my-mmd-basic-biblatex-eddbib 
    latex input:        my-mmd-basic-begin-doc  
    latex footer:       my-mmd-basic-biblatex-footer
      
    <!--\include{exported}-->

### BibTeX example

Example markdown document:


    latex input:        my-mmd-basic-header  
    Title:              My Document  
    Author:             Stephen Murphy  
    Keywords:           tests, thesis  
    Base Header Level:  2  
    LaTeX Mode:         memoir  
    latex input:        my-mmd-basic-setup  
    latex input:        my-mmd-basic-bibtex  
    latex input:        my-mmd-basic-begin-doc  
    latex footer:       my-mmd-basic-bibtex-footer  
      
    <!--\include{exported}-->


# License

All files are written by and copyright Stephen Murphy <mailto:stephen.j.murphy@student.uts.edu.au>, although there have been many influences over the years affecting the final result.

Any and all of the author's rights are currently reserved.